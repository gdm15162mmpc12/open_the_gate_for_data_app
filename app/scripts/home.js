/* HOME FORM */

document.getElementById('frmHome').addEventListener("submit", function(evt){
    evt.preventDefault();

    if(formHomevalidation()) {
        var name = document.getElementById("name").value;
        sessionStorage.setItem("urname", name);
        relativeRedir('country.html');
    }

    return false;
}, true);

function formHomevalidation() {
    var name = document.getElementById("name").value;
    if (name === '') {
        document.getElementById("no_name").innerHTML = "Type name in box below";
        return false;
    }
    return true;
}

function relativeRedir(redir){
    location.pathname = location.pathname.replace(/(.*)\/[^/]*/, "$1/"+redir);
}




