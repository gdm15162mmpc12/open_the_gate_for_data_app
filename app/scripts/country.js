/* RETRIEVE NAME */

var name = sessionStorage.getItem("urname");
if( name != 'null') {
    document.getElementById("input_name").innerHTML = name;
} else {
    location.pathname = location.pathname.replace(/(.*)\/[^/]*/, "$1/" + 'index.html');
}


/* FORM COUNTRY */

document.getElementById('frmCountry').addEventListener("submit", function(evt){
    evt.preventDefault();

    if(fromCountryvalidation()) {
        var country = $("option:selected").text();
        sessionStorage.setItem("urcountry", country);
        relativeRedir('result.html');
    }
    return false;
}, true);

function fromCountryvalidation() {
    var country = document.getElementById("country").value;
    if (country === '') {
        document.getElementById("no_country").innerHTML = "Choose your country in box below";
        return false;
    }
    return true;
}

function relativeRedir(redir){
    location.pathname = location.pathname.replace(/(.*)\/[^/]*/, "$1/"+redir);
}




