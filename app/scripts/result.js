// RETRIEVE COUNTR

var country = sessionStorage.getItem("urcountry");
if( country != 'null') {
    document.getElementById("input_country").innerHTML = country;
} else {
    location.pathname = location.pathname.replace(/(.*)\/[^/]*/, "$1/" + 'country.html');
}

// INLADEN DATASETS

var countryDataSet = {
    "pollution": null,
    "recycle": null,
    "water": null,
    "electricity": null,
    "cars": null,
    "forest": null
};

var countryCode = 'BE';


// CO2-POLLUTION

var WBCO2POLLUTIONAPI = 'http://api.worldbank.org/countries/' + countryCode + '/indicators/EN.ATM.CO2E.PC?format=jsonP&prefix=?&per_page=100';

function loadCo2(countryCode) {

    var myajax_nonce = jQuery.now();
    jQuery.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function() {
            var callback = ( jQuery.expando.toLowerCase() + "_" + ( myajax_nonce++ ) );
            this[ callback ] = true;
            return callback;
        }
    });

    $.ajax({
        url: WBCO2POLLUTIONAPI,
        dataType: "jsonp",
        success: function( data ) {
            countryDataSet.pollution = data[1];
            console.log( countryDataSet ); // server response
        }
    });
}

loadCo2('be');


// RENEWABLE ENERGY CONSUMPTION

var WBRECYCLEAPI = 'http://api.worldbank.org/countries/' + countryCode + '/indicators/EG.FEC.RNEW.ZS?format=jsonP&prefix=?&per_page=100';

function loadRecycle(countryCode) {

    var myajax_nonce = jQuery.now();
    jQuery.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function() {
            var callback = ( jQuery.expando.toLowerCase() + "_" + ( myajax_nonce++ ) );
            this[ callback ] = true;
            return callback;
        }
    });

    $.ajax({
        url: WBRECYCLEAPI,
        dataType: "jsonp",
        success: function( data ) {
            countryDataSet.recycle = data[1];
            console.log( countryDataSet ); // server response
        }
    });
}

loadRecycle(countryCode);


// DOMESTIC FRESHWATER WITHDRAWALS

var WBWATERAPI = 'http://api.worldbank.org/countries/' + countryCode + '/indicators/ER.H2O.FWDM.ZS?format=jsonP&prefix=?&per_page=100';

function loadWater(countryCode) {

    var myajax_nonce = jQuery.now();
    jQuery.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function() {
            var callback = ( jQuery.expando.toLowerCase() + "_" + ( myajax_nonce++ ) );
            this[ callback ] = true;
            return callback;
        }
    });

    $.ajax({
        url: WBWATERAPI,
        dataType: "jsonp",
        success: function( data ) {
            countryDataSet.water = data[1];
            console.log( countryDataSet ); // server response
        }
    });
}

loadWater('be');


// ELECTRIC POWER CONSUMPTION

var WBELECTRICITYAPI = 'http://api.worldbank.org/countries/' + countryCode + '/indicators/EG.USE.ELEC.KH.PC?format=jsonP&prefix=?&per_page=100';

function loadElectricity(countryCode) {

    var myajax_nonce = jQuery.now();
    jQuery.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function() {
            var callback = ( jQuery.expando.toLowerCase() + "_" + ( myajax_nonce++ ) );
            this[ callback ] = true;
            return callback;
        }
    });

    $.ajax({
        url: WBELECTRICITYAPI,
        dataType: "jsonp",
        success: function( data ) {
            countryDataSet.electricity = data[1];
            console.log( countryDataSet ); // server response
        }
    });
}

loadElectricity('be');


// AMOUNT OF CARS

var WBCARSAPI = 'http://api.worldbank.org/countries/' + countryCode + '/indicators/EG.USE.ELEC.KH.PC?format=jsonP&prefix=?&per_page=100';

function loadCars(countryCode) {

    var myajax_nonce = jQuery.now();
    jQuery.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function() {
            var callback = ( jQuery.expando.toLowerCase() + "_" + ( myajax_nonce++ ) );
            this[ callback ] = true;
            return callback;
        }
    });

    $.ajax({
        url: WBCARSAPI,
        dataType: "jsonp",
        success: function( data ) {
            countryDataSet.cars = data[1];
            console.log( countryDataSet ); // server response
        }
    });
}

loadCars('be');


// FOREST AREA

var WBFORESTAREAAPI = 'http://api.worldbank.org/countries/' + countryCode + '/indicators/AG.LND.FRST.ZS?format=jsonP&date=1990:2013&per_page=300&prefix=?';

function loadForest(countryCode) {

    var myajax_nonce = jQuery.now();
    jQuery.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function() {
            var callback = ( jQuery.expando.toLowerCase() + "_" + ( myajax_nonce++ ) );
            this[ callback ] = true;
            return callback;
        }
    });

    $.ajax({
        url: WBFORESTAREAAPI,
        dataType: "jsonp",
        success: function( data ) {
            countryDataSet.forest = data[1];
            console.log( countryDataSet ); // server response
        }
    });
}

loadForest('be');


// EINDE DATASETS INLADEN